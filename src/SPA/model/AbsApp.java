package SPA.model;

import java.util.HashMap;
import java.util.List;

import patterns.observer.Observed;
import patterns.observer.ObserverCode;
import SPA.API.IClienteSistemaPago;
import SPA.API.ICobrable;

public abstract class AbsApp extends Observed implements ICobrable

{	
	private boolean estado;//exito o no
	protected ServicioPago servicio;

	//atributos del pago 
	private int cantidad;
	protected String metodoPago;
	private List<String> credenciales;
	private IClienteSistemaPago cliente;
	@Override
	public void cobrar(int cantidad,IClienteSistemaPago cliente){
		this.cantidad = cantidad;
		this.cliente = cliente;
		//lanzar
	}
	public void lanzar(){
		//muestra las vistas asociadas a este modelo
		this.notify_observers(ObserverCode.LAUNCH);
	}


	public boolean isSuccess(){
		return estado;
	}
	public void iniciar() {
		crearServicio();
		ObserverCode  code = servicio.cobrar(cantidad, credenciales);
		if(code.equals(ObserverCode.PAYSUCCESS))
			estado = true;
		else
			estado = false;
		this.notify_observers(code);
	}
				
	public void seleccionarMetodoPago(String metodo){
		this.metodoPago = metodo;
	}
	public void cargarCredenciales(List<String> credenciales){
		this.credenciales = credenciales;
	}
		
	public void abortar() {
		//this.estado = false;
		this.cliente.notificar(this);				
	}

	public HashMap<String, Class> getCredencialesNecesarias() {
		
		crearServicio();
		return servicio.getCredencialesNecesarias();
		
	}

	public abstract List<String> getTiposMetodosDePago();//devuele una lista con los metodos de pago disponibles.
	
	public abstract void crearServicio();//metodo fabrica
}
