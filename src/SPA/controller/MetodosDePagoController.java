package SPA.controller;



import patterns.observer.Observed;

import AbsControllers.ItemChangeController;
import SPA.model.App;
import SPA.view.FormCredencialesPago;
import SPA.view.VMetodosDePago;


public class MetodosDePagoController extends ItemChangeController

{

	private VMetodosDePago view;
	private App app;
	
	public MetodosDePagoController(VMetodosDePago aView, Observed aModel) {
		super();
        view = aView;
        app = (App) aModel;

	}
	@Override
	public void actualizar() {
		
		String metodoPago = view.getMetodoPago();
		app.seleccionarMetodoPago(metodoPago);
		
		FormCredencialesPago from = view.getFormCredenciales();
		from.actualizar(app, metodoPago);
		
	}

	@Override
	protected boolean isValid() {
		return !view.getMetodoPago().isEmpty();
	}
}

